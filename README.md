# _μot_ [/my:oʊ:ti/] or more formally myot

> !!WARNING! This project is just for fun and not under active development.

## The IOT protocol

##### Table of Contents:
- [Basic Introduction](#basic-introduction)
- [Origin](#origin)
- [Protocol](#protocol)
- [Code Examples](#code-examples)
    - [JavaScript](#javascript)
    - [Java](#java)
    - [Python](#python)
    - [Swift](#swift)
- [License](#license)

### Basic Introduction
_μot_ is _the_ protocol for IOT devices. It's a small, lightweight and fast but fully-fledged, device independent standard for communication between IOT devices. 

### Origin
The name _μot_ ([_pronounciation_](./muot.ogg)) is composed of two parts: the [μ](https://en.wikipedia.org/wiki/Mu_\(letter\)) (`U+03BC`) which was the Ancient Greek's **M** and now is the SI-Prefix [_micro-_](https://en.wikipedia.org/wiki/Micro-) which represents one millionth (`10e-6`) and the [ot](https://en.wikipedia.org/wiki/Internet_of_things) which stands for the abbreviation [IOT](https://en.wikipedia.org/wiki/Internet_of_things) (Internet of Things).

The project is Open Source and it's current contributors are:
- [Loïc Nestler](https://loicnestler.com/)

### Protocol

#### Structure

Each packet is up to 8 Bytes long

|   Name  | [Handshake](#handshake) |  [Needs Response](#needs-response) | [Type](#type)  | [Value-Length](#value-length) | [Value](#values) 1 | Value 2 | Value 3 | Value 4 |
|:-------:|:---------:|:------:|:------------:|:-------:|:-------:|:-------:|:-------:|:--------------:|
| Example |   `0x01`  | `0x01` |     `0x2d`     |    `0x02`    |  `0x00` |  `0x24` |         |         |

###### Handshake
The handshake is the start of every packet.
Possible values are `0x01` and `0x02`.

<style>
.messageText {
    font-family: monospace;
}
</style>
```mermaid
sequenceDiagram
    participant C as Client
    participant S as Server

    C ->> S: 0x0001
    S ->> C: 0x0002
```

### Code Examples

#### Javascript
```javascript
const myot, {build} = require('myot')

/**
 * Creates a new myot server and sets up basic handshake procedure 
 */
const server = new myot.Server()

server.on('connect', client => {
    client.send(build(0x01, 0x01, 0x00, true)).expect(response => {
        console.log(`[Server] Recieved handshake-response ${response}`)
    })

    // OR

    client.handshake().then(response => {
        console.log(`[Server] Recieved handshake-response ${response}`)
    })
})


/**
 * Creates a new client and listens on handshakes
 */
const client = new myot.Client('127.0.0.1')

// Handshake-responses are handled automatically by myot
client.on('handshake', data => {
    console.log('[Client] Recieved handshake')
})
```

#### Java
```java

public class ConnectListener implements Listener {

    @EventHandler
    public void onConnect(client) {
        client.handshake();
    }

}

public class Main {

    public static void main(String... args) {
        Server server = new Server();
        server.registerEvents(new ConnectListener(), this);

        Client client = new Client("127.0.0.1");
        client.registerEvents(new HandshakeListener(), this);
    }

}

public class HandshakeListener implements Listener {

    @EventHandler
    public void onHandshake(data) {
        System.out.println("Recieved handshake " + data);
    }

}

```

#### Python
```python
print("coming soon")
```

#### Swift
```swift
print("coming soon")
```

### License
> MIT License
> 
> Copyright (c) 2018 Loïc Nestler
> 
> Permission is hereby granted, free of charge, to any person obtaining a copy
> of this software and associated documentation files (the "Software"), to deal
> in the Software without restriction, including without limitation the rights
> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
> copies of the Software, and to permit persons to whom the Software is
> furnished to do so, subject to the following conditions:
> 
> The above copyright notice and this permission notice shall be included in all
> copies or substantial portions of the Software.
> 
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
> SOFTWARE.